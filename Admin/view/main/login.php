<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="robots" content="noindex,nofollow" />
<title>登录</title>
<link href="<?=ADMIN_CSS?>/bootstrap.min.css" rel="stylesheet">
<link type="text/css" href="<?=ADMIN_CSS?>/font-awesome.min.css" rel="stylesheet" />
<link type="text/css" href="<?=ADMIN_CSS?>/jquery.dataTables.min.css" rel="stylesheet" />
<!--[if IE 7]>
<link rel="stylesheet" href="<?=ADMIN_CSS?>/font-awesome-ie7.min.css">
<![endif]-->
<link type="text/css" href="<?=ADMIN_CSS?>/jquery-ui-1.10.0.custom.css" rel="stylesheet" />
<link rel="stylesheet" href="<?=ADMIN_CSS?>/style.css">
<?php if(isset($require_js)):?>
<script language="javascript" type="text/javascript"> var SITE_URL = "<?=ADMIN_JS?>";</script>
<script src="<?=ADMIN_JS?>/require.js" ></script>
<?php else:?>
<script src="<?=ADMIN_JS?>/lib/jquery.js" ></script>
<script src="<?=ADMIN_JS?>/lib/jquery-ui-1.10.0.custom.min.js"></script>
<script src="<?=ADMIN_JS?>/lib/jquery.datetimepicker.js"></script>
<script src="<?=ADMIN_JS?>/lib/jquery.validationEngine-zh_CN.js" ></script>
<script src="<?=ADMIN_JS?>/lib/jquery.validationEngine.js" ></script>
<script src="<?=ADMIN_JS?>/lib/global.js"></script>
<?php endif;?>
 <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="<?=ADMIN_CSS?>/ie8-responsive-file-warning.js"></script><![endif]-->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="http://cdn.bootcss.com/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="http://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body >
<div id="dialog" style="padding:0px;"></div>

<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <a href="/" ><div class="tsLogo "></div></a>
            </div>
            <div class="col-md-6">
            </div>
        </div>
    </div>
</div>
<div class="container">
    <form class="form-signin" role="form" action="/admin.php/main/login"  method="post" id="validateform" name="validateform">
        <div class="form-signin-body">

            <div class="form-group">
                <span class="input-group-addon" ><i class="glyphicon glyphicon-user"></i></span>
                <input type="text" id="username" name="username" class="form-control" placeholder="请输入管理帐号" autofocus>
            </div>

            <div class="form-group">
                <span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-lock"></i></span>
                <input type="password" id="password" name="password" class="form-control" placeholder="请输入管理密码"  autofocus>
            </div>

            <button class="btn  btn-primary btn-block" type="submit" id="dosubmit" disabled="disabled"> 登录</button>

            <div class="checkbox">
                <label>
                    <input type="checkbox" id="rmbUser" value="remember-me"> 在此设备上保存登录
                </label>
            </div>
        </div>
        <div class="form-signin-footer"> <a><i class="glyphicon glyphicon-question-sign"></i> 忘记密码？</a></div>
    </form>

    <script language="javascript" type="text/javascript">
        require(['<?=ADMIN_JS?>/common.js'], function (common) {
            require(['<?=ADMIN_JS?>/login.js']);
        });
    </script>
</div>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="http://v3.bootcss.com/assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>