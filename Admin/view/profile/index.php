<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <title>首页</title>
    <meta name="keywords" content="bootstrap3" />
    <meta name="description" content="欢迎来访~" />
    <meta name="version" content="v 3.0" />
    <meta name="author" content="long"/>
    <!--移动设备优先-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--[if lt IE 9]>
    <script>window.location.href= "/error.html";</script>
    <![endif]-->
    <link rel="stylesheet" href="<?=CSS?>/bootstrap.css" />
    <link rel="stylesheet" href="<?=CSS?>/my.css" />
    <link rel="stylesheet" href="<?=CSS?>/font_1477105914_3430886.css">

    <script src="<?=JS?>/jquery-1.10.1.min.js" ></script>
</head>
<body>


<div class="box">

    <!--头部开始-->
    <div class="row header">
        <div class=" mbd">
            <div class="col-lg-2 col-md-2 header-logo">
                <a title="" href="index.html"><img src="<?=IMG?>/logo.jpg" /></a>

            </div>
            <div class="header-menu col-lg-3 col-md-3 col-xs-12">
                <nav class="navbar navbar-default">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">导航</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand visible-xs" href="#">Welcome to Visit</a>
                    </div>
                    <div class="collapse navbar-collapse menu" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="menu-active"><a href="index.html">About Us</a></li>
                            <li class=""><a href="about.html">News</a></li>
                            <li class=""><a href="links.html">Sign In</a></li>

                        </ul>
                        <div class="menu-bar"></div>
                        <div class="menu-clean"></div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
    <!--头部结束-->
    <div class="row profile">
        <h2><img src="<?=IMG?>/kol_profilepic_01.png" class="propic"></h2>
        <h3>亞北本人</h3>
        <h3><a href="#" class="prbt">Invite</a><a href="#" class="prbt">Save</a></h3>
    </div>
</div>
<div class="container">
    <div class="row aerousel">

        <div class="col-md-12 row-left index">

            <div class="nbox-all">
                <div class="nbox">
                    <div class="col-lg-9 pgif ">
                        <div class="pdiv">
                            <img src="<?=IMG?>/p3.jpg">
                            <h3 class="propd">文章標題 <span class="rt"><img src="<?=IMG?>/f3.png" class="rtpic"><img src="<?=IMG?>/f2.png" class="rtpic"><img src="<?=IMG?>/f1.png" class="rtpic"></span></h3>
                            <li  class="propd">内容内容内容内容内容内容内容内容内容内容内容内容内容内容</li>
                        </div> <div class="pdiv">
                            <img src="<?=IMG?>/p3.jpg">
                            <h3  class="propd">文章標題 <span class="rt"><img src="<?=IMG?>/f3.png" class="rtpic"><img src="<?=IMG?>/f2.png" class="rtpic"><img src="<?=IMG?>/f1.png" class="rtpic"></span></h3>
                            <li  class="propd">内容内容内容内容内容内容内容内容内容内容内容内容内容内容</li>
                        </div> <div class="pdiv">
                            <img src="<?=IMG?>/p3.jpg">
                            <h3  class="propd">文章標題 <span class="rt"><img src="<?=IMG?>/f3.png" class="rtpic"><img src="<?=IMG?>/f2.png" class="rtpic"><img src="<?=IMG?>/f1.png" class="rtpic"></span></h3>
                            <li  class="propd">内容内容内容内容内容内容内容内容内容内容内容内容内容内容</li>
                        </div> <div class="pdiv">
                            <img src="<?=IMG?>/p3.jpg">
                            <h3  class="propd">文章標題 <span class="rt"><img src="<?=IMG?>/f3.png" class="rtpic"><img src="<?=IMG?>/f2.png" class="rtpic"><img src="<?=IMG?>/f1.png" class="rtpic"></span></h3>
                            <li  class="propd">内容内容内容内容内容内容内容内容内容内容内容内容内容内容</li>
                        </div> <div class="pdiv">
                            <img src="<?=IMG?>/p3.jpg">
                            <h3  class="propd">文章標題 <span class="rt"><img src="<?=IMG?>/f3.png" class="rtpic"><img src="<?=IMG?>/f2.png" class="rtpic"><img src="<?=IMG?>/f1.png" class="rtpic"></span></h3>
                            <li  class="propd">内容内容内容内容内容内容内容内容内容内容内容内容内容内容</li>
                        </div> <div class="pdiv">
                            <img src="<?=IMG?>/p3.jpg">
                            <h3  class="propd">文章標題 <span class="rt"><img src="<?=IMG?>/f3.png" class="rtpic"><img src="<?=IMG?>/f2.png" class="rtpic"><img src="<?=IMG?>/f1.png" class="rtpic"></span></h3>
                            <li  class="propd">内容内容内容内容内容内容内容内容内容内容内容内容内容内容</li>
                        </div>
                    </div>
                    <div class="col-lg-3 npgifl ">
                        <h2>中國香港 12歲 女<br>創作系 寵物系</h2>
                        <p class="bdc">粉絲量</p>
                        <li><img src="<?=IMG?>/f3.png" class="rtpic nmb"> 3,000</li>
                        <li><img src="<?=IMG?>/f2.png" class="rtpic nmb"> 3,000</li>
                        <li><img src="<?=IMG?>/f1.png" class="rtpic nmb"> 3,000</li>
                    </div>
                </div>

            </div>

        </div>

    </div>
</div>


<script src="<?=JS?>/layer.js"></script>
<script src="<?=JS?>/bootstrap.js"></script>
<script src="<?=JS?>/common.js"></script>
</body>
</html>