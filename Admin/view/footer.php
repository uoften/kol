<div class="foot" style="text-align:center;">
    <div class="col-md-12">

        <ul class="list-inline">


            <li><a class="link-footer" href="http://www.freelancepro.hk/help">Help</a></li>


            <li><a class="link-footer" href="http://www.freelancepro.hk/terms-of-service">Terms of Service</a></li>


            <li><a class="link-footer" href="http://www.freelancepro.hk/privacy">Privacy</a></li>


            <li><a class="link-footer" href="http://www.freelancepro.hk/advertising">Advertise with us</a></li>


            <li><a class="link-footer" href="http://www.freelancepro.hk/about">About Us</a></li>


            <li><a class="link-footer" href="http://www.freelancepro.hk/pro">Be Pro</a></li>


            <li><a class="link-footer" href="http://www.freelancepro.hk/api">API</a></li>



            <!-- Start Languages -->

            <li>

                <a class="link-footer" href="http://www.freelancepro.hk/lang/en">English</a>

            </li>

            <!-- Start Languages -->

            <li>

                <a class="link-footer" href="http://www.freelancepro.hk/lang/zh">中文</a>

            </li>




            <!-- ./End Languages -->



        </ul>

    </div>
</div>

<script src="<?=JS?>/bootstrap.js"></script>
<script src="<?=JS?>/common.js"></script>


</body>
</html>