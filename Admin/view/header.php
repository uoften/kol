<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <title>首页</title>
    <meta name="keywords" content="bootstrap3" />
    <meta name="description" content="欢迎来访~" />
    <meta name="version" content="v 3.0" />
    <meta name="author" content="long"/>
    <!--移动设备优先-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--[if lt IE 9]>
    <script>window.location.href= "/error.html";</script>
    <![endif]-->
    <script src="<?=JS?>/jquery-1.10.1.min.js" ></script>
    <script type="text/javascript" src="<?=JS?>/jquery.bxslider.js"></script>
    <link rel="stylesheet" href="<?=CSS?>/bootstrap.css" />
    <link rel="stylesheet" href="<?=CSS?>/my.css" />
    <link rel="stylesheet" href="<?=CSS?>/font_1477105914_3430886.css">
    <link href="<?=CSS?>/jquery.bxslider.css" rel="stylesheet" type="text/css">

</head>
<body>


<div class="box">

    <!--头部开始-->
    <div class="row header">
        <div class=" mbd">
            <div class="col-lg-2 col-md-2 header-logo">
                <a title="" href="index.html"><img src="<?=IMG?>/logo.jpg" /></a>

            </div>
            <div class="header-menu col-lg-3 col-md-3 col-xs-12">
                <nav class="navbar navbar-default">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">导航</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand visible-xs" href="#">Welcome to Visit</a>
                    </div>
                    <div class="collapse navbar-collapse menu" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="menu-active"><a href="/admin.php/main/info">关于</a></li>
                            <li class=""><a href="/admin.php/main/news">活动</a></li>
                            <li class=""><a href="/admin.php/main/index"><?=$userdata['fullname']?></a></li>
                            <li class=""><a href="/index.php/main/logout">注销</a></li>

                        </ul>
                        <div class="menu-bar"></div>
                        <div class="menu-clean"></div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
    <!--头部结束-->
    <div class="row profile  mbg ipr">
        <h4>A brand discovering journal for KOL</h4>
        <h3><a href="#" class="fbt">Find KOL</a><a href="#" class="fbt">Find Campaign</a></h3>
        <div class="btwid"> <span><a class="bt1" href="#">&nbsp;</a> </span><span><a class="bt2" href="#">&nbsp;</a> </span><span><a class="bt3" href="#">&nbsp;</a> </span>
        </div>
    </div>

</div>