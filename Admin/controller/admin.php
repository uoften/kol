<?php
class Admin_controller_admin extends spController
{
	public $user_id,$user_name,$group_id;

	public function __construct(){
		parent::__construct();
		define("IN_ADMIN", TRUE);
		$this->user_id = intval(userdata('user_id'));
		$this->user_name = userdata('user_name');
		$this->group_id = intval(userdata('group_id'));
		$this->check_member();
	}

	public function showmessage($msg, $url_forward = '', $ms = 500, $dialog = '', $returnjs = '') {
		if($url_forward=='')$url_forward=$_SERVER['HTTP_REFERER'];
		$datainfo = array("msg"=>$msg,"url_forward"=>$url_forward,"ms"=>$ms,"returnjs"=>$returnjs,"dialog"=>$dialog);
		foreach ($datainfo as $k=>$v)
		{
			$this->$k = $v;
		}
		$this->display("common/message.php");
		exit;
	}

	/**
	 * 判断用户是否已经登陆
	 */
	public function check_member() {
		if(!$this->user_id)
		{
			$this->showmessage('请您重新登录',site_url('index.php/main/login'));
			exit(0);
		}
		$_datainfo = spClass('users')->find(array('user_id'=>$this->user_id,'username'=>$this->user_name));
		if(!$_datainfo)
		{
			$this->showmessage('请您重新登录',site_url('index.php/main/login'));
			exit(0);
		}else{
			$this->current_member_info = $_datainfo;
		}
	}
}