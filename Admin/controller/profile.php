<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/9/6
 * Time: 21:36
 */

class profile extends spController
{


    /**
     * 封面
     * http://git.kol.com/index.php?c=profile
     */
    public function index()
    {
        $this->display("profile/index.php");
    }


    /**
     * 详情
     * http://git.kol.com/index.php?c=profile&a=detail
     */
    public function detail()
    {
        $this->display("profile/detail.php");
    }
}