<?php
/**
 * 跃飞科技版权所有 @2017
 */
define("SP_PATH",dirname(__FILE__)."/SpeedPHP");
define('APP_PATH', './Admin/');
define('Common_PATH', './Common/');
$spConfig = require(APP_PATH."/config/config.php"); //spConfigReady函数可以覆盖用户配置项
require(SP_PATH."/SpeedPHP.php");
//require(Common_PATH."/config/constants.php");
//require(Common_PATH."/helper/function.php");
require  './vendor/autoload.php';
header('content-type:text/html;charset=utf-8');
spRun();