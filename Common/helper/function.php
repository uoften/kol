<?php
/**
 * 跃飞科技版权所有 @2017
 * User: 钟贵廷
 * Date: 2017/9/7
 * Time: 15:30
 */

if(!function_exists('p'))
{
    /**
     * 格式化打印函数
     * @param  [type] $arr [数组]
     * @return [type]      [description]
     */
    function p($arr)
    {
        echo '<pre style="color: red;">';
        print_r($arr);
        echo '</pre>';
    }
}

if(!function_exists('site_url'))
{
	function site_url($uri = '')
	{
		return WEB_SIZE.'/'.$uri;
	}
}



if(!function_exists('set_userdata'))
{
	/*
	 * 设置SESSION
	 * $model->set_userdata('user_name',$username)
	 */
	function set_userdata($data, $value = NULL)
	{
		if (is_array($data))
		{
			foreach ($data as $key => &$value)
			{
				$_SESSION[$key] = $value;
			}
	
			return;
		}
	
		$_SESSION[$data] = $value;
	}
}

if(!function_exists('gt_autoload'))
{
	/**
	 * 自动加载类库
	 * 类名规范:文件夹_类名
	 * @param $class_name
	 */
	function gt_autoload($class_name)
	{
		//Faker\Provider\en_US\Barcode (这里是composer下载下来的一个模拟生成数据的类库，这里出错了)
		if(strpos($class_name,'\\' ) === false)
		{
			$file = str_replace('_', '/', $class_name). '.php';
			if(file_exists($file))
			{
				require $file;
			}
			else
			{
				die ($file.'文件不存在，请检查类名是否规范:文件夹_类名');
			}
		}

	}
}

//注册函数，让类库自动加载
spl_autoload_register( 'gt_autoload' );


if ( ! function_exists('unset_userdata'))
{
	function unset_userdata($key)
	{
		if (is_array($key))
		{
			foreach ($key as $k)
			{
				unset($_SESSION[$k]);
			}
	
			return;
		}
	
		unset($_SESSION[$key]);
	}
}

if ( ! function_exists('get_userdata'))
{
	function &get_userdata()
	{
		return $_SESSION;
	}
}

if ( ! function_exists('userdata'))
{
	function userdata($key = NULL)
	{
		if (isset($key))
		{
			return isset($_SESSION[$key]) ? $_SESSION[$key] : NULL;
		}
		elseif (empty($_SESSION))
		{
			return array();
		}
	
		$userdata = array();
		$_exclude = array_merge(
				array('__ci_vars'),
				get_flash_keys(),
				get_temp_keys()
		);
	
		foreach (array_keys($_SESSION) as $key)
		{
			if ( ! in_array($key, $_exclude, TRUE))
			{
				$userdata[$key] = $_SESSION[$key];
			}
		}
	
		return $userdata;
	}
}

if ( ! function_exists('get_flash_keys'))
{
	function get_flash_keys()
	{
		if ( ! isset($_SESSION['__ci_vars']))
		{
			return array();
		}
	
		$keys = array();
		foreach (array_keys($_SESSION['__ci_vars']) as $key)
		{
			is_int($_SESSION['__ci_vars'][$key]) OR $keys[] = $key;
		}
	
		return $keys;
	}
}

if ( ! function_exists('get_temp_keys'))
{
	function get_temp_keys()
	{
		if ( ! isset($_SESSION['__ci_vars']))
		{
			return array();
		}
	
		$keys = array();
		foreach (array_keys($_SESSION['__ci_vars']) as $key)
		{
			is_int($_SESSION['__ci_vars'][$key]) && $keys[] = $key;
		}
	
		return $keys;
	}
}

if ( ! function_exists('redirect'))
{
	function redirect($uri = '', $method = 'auto', $code = NULL)
	{
		if ( ! preg_match('#^(\w+:)?//#i', $uri))
		{
			$uri = site_url($uri);
		}

		// IIS environment likely? Use 'refresh' for better compatibility
		if ($method === 'auto' && isset($_SERVER['SERVER_SOFTWARE']) && strpos($_SERVER['SERVER_SOFTWARE'], 'Microsoft-IIS') !== FALSE)
		{
			$method = 'refresh';
		}
		elseif ($method !== 'refresh' && (empty($code) OR ! is_numeric($code)))
		{
			if (isset($_SERVER['SERVER_PROTOCOL'], $_SERVER['REQUEST_METHOD']) && $_SERVER['SERVER_PROTOCOL'] === 'HTTP/1.1')
			{
				$code = ($_SERVER['REQUEST_METHOD'] !== 'GET')
				? 303	// reference: http://en.wikipedia.org/wiki/Post/Redirect/Get
				: 307;
			}
			else
			{
				$code = 302;
			}
		}

		switch ($method)
		{
			case 'refresh':
				header('Refresh:0;url='.$uri);
				break;
			default:
				header('Location: '.$uri, TRUE, $code);
				break;
		}
		exit;
	}
}
