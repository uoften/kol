<?php
/**
 * 使用pdo封装自己的类库
 * Class Common_model_gtModel
 */
class Common_model_gtModel
{
    //pdo对象
    private $_pdo = null;
    //用于存放实例化的对象
    static private $_instance = null;

//    表名
    protected $table;

    private $dsn = null;
    //公共静态方法获取实例化的对象
    static protected function getInstance()
    {
        if (!(self::$_instance instanceof self))
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    //私有克隆
    private function __clone() {}

    /**
     * 设置没有定义属性的变量
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {

        $this->$name = $value;
    }

    /**
     * 获取没有属性的变量
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        return $this->$name;
    }

    //私有构造
    private function __construct()
    {
        $db = $GLOBALS['G_SP']['db'];
        $this->dsn = "{$db['driver']}:dbname={$db['database']};host={$db['host']}";
        try
        {
            $this->_pdo = new PDO($this->dsn, $db['login'], $db['password'], array(PDO::MYSQL_ATTR_INIT_COMMAND=>'SET NAMES UTF8'));
            $this->_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch (PDOException $e)
        {
            exit($e->getMessage());
        }
    }

    /**
     * 查找一张表的所有数据
     * @param $table  --表名
     * @return array
     */
    public function select()
    {
        $sql = 'select * from '.$this->table;
        $rs = $this->_pdo->query($sql);
        $arr = [];
        while($row = $rs->fetch())
        {
           $arr[] = $row;
        }
        return $arr;
    }


    /**
     * 添加一条数据
     * @param $table
     * @param $data
     * @return int
     */
    public function add($data)
    {
        $key = implode('`,`',array_keys($data) );
        $value = implode("','",array_values($data));
        $sql = "insert into {$this->table} (`$key`) values ('$value')";
        $stmt = $this->execute($sql);
        return $stmt->rowCount();
    }

    /**
     * 修改一条数据
     * @param $data
     * @param $condition
     */
    public function save($data,$condition)
    {
        //update users set `username`='xx' WHERE `user_id` = '10' and `xxx`='xxx'
        $str = '';
        foreach ($data as $k=>$v)
        {
            $str .= "`{$k}`='{$v}',";
        }
        $str = substr($str,0,-1);

        $where = '';
        foreach ($condition as $k=>$v)
        {
            $where .= "`{$k}`='{$v}' and ";
        }
        $where = substr($where,0,-5);

        $sql = "UPDATE {$this->table} SET $str WHERE $where ";
        $stmt = $this->execute($sql);
        return $stmt->rowCount();
    }

    /**
     * 执行sql
     * @param $sql
     * @return PDOStatement
     */
    private function execute($sql)
    {
        try
        {
            $stmt = $this->_pdo->prepare($sql);
            $stmt->execute();
        }
        catch (PDOException  $e)
        {
            exit('SQL语句：'.$sql.'<br />错误信息：'.$e->getMessage());
        }
        return $stmt;
    }

}
?>
