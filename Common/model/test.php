<?php
/**
 * 跃飞科技版权所有 @2017
 * User: 钟贵廷
 * Date: 2017/9/8
 * Time: 13:15
 */
class Common_model_test extends Common_model_gtModel
{
    public $db;

    public function __construct()
    {
         $this->db = parent::getInstance();
         $this->db->table = 'kol_users';
    }

    public function getUser()
    {
       $user = $this->db->select();
       return $user;
    }

    public function insertUser()
    {
        $data = [
            'username'=>'test002',
            'password'=>'test001'
        ];
        $res = $this->db->add($data);
        p($res);
    }

    public function updateUser()
    {
        $data = [
            'username'=>'test005',
            'password'=>'test001'
        ];
        return  $this->db->save($data,['user_id'=>5]);
    }

}