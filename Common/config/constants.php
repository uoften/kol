<?php
/**
 * 跃飞科技版权所有 @2017
 * User: 扩展常量
 * Date: 2017/3/14
 * Time: 10:01
 */

defined('WEB_SIZE') OR define('WEB_SIZE', 'http://'.@$_SERVER['SERVER_NAME']);
//后台样式
defined('ADMIN_CSS') OR define('ADMIN_CSS',WEB_SIZE.'/assets/admin/styles');
defined('ADMIN_JS') OR define('ADMIN_JS',WEB_SIZE."/assets/admin/js");
defined('ADMIN_IMG') OR define('ADMIN_IMG',WEB_SIZE."/assets/admin/images");

//前台样式
defined('CSS') OR define('CSS',WEB_SIZE."/assets/home/css");
defined('JS') OR define('JS',WEB_SIZE."/assets/home/js");
defined('IMG') OR define('IMG',WEB_SIZE."/assets/home/images");
defined('FONT') OR define('FONT',WEB_SIZE."/assets/home/fonts");