<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <title>首页</title>
    <meta name="keywords" content="bootstrap3" />
    <meta name="description" content="欢迎来访~" />
    <meta name="version" content="v 3.0" />
    <meta name="author" content="long"/>
    <!--移动设备优先-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--[if lt IE 9]>
    <script>window.location.href= "/error.html";</script>
    <![endif]-->
    <link rel="stylesheet" href="<?=CSS?>/bootstrap.css" />
    <link rel="stylesheet" href="<?=CSS?>/my.css" />
    <link rel="stylesheet" href="<?=CSS?>/font_1477105914_3430886.css">

    <script src="<?=JS?>/jquery-1.10.1.min.js" ></script>
</head>
<body>


<div class="box">

    <!--头部开始-->
    <div class="row header">
        <div class=" mbd">
            <div class="col-lg-2 col-md-2 header-logo">
                <a title="" href="index.html"><img src="<?=IMG?>/logo.jpg" /></a>

            </div>
            <div class="header-menu col-lg-3 col-md-3 col-xs-12">
                <nav class="navbar navbar-default">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">导航</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand visible-xs" href="#">Welcome to Visit</a>
                    </div>
                    <div class="collapse navbar-collapse menu" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="menu-active"><a href="index.html">About Us</a></li>
                            <li class=""><a href="about.html">News</a></li>
                            <li class=""><a href="links.html">Sign In</a></li>

                        </ul>
                        <div class="menu-bar"></div>
                        <div class="menu-clean"></div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
    <!--头部结束-->
</div>
<div class="container">
    <div class="row aerousel">

        <div class="col-md-12 row-left index">

            <div class="get-all">
                <div class="get">
                    <div class="col-lg-2 red cc">關於我們</div>
                    <div class="col-lg-10">
                        <p>ParkLU是一個致力於拓展品牌與中國風尚達人之間合作互動的先鋒數字平台.</p>
                        <p>自一開始，我們就為品尚品牌與電子商務-包括財富100強修鍊和獨立品牌-作好和中國頂尖風尚達人合作的鋪墊。
                            我們深信那些能夠輕易透過一張圖片或一段文字啟發朋友與粉絲的數字創意群體是當代社會不二的"說書"人選；
                            2011年由Kim Leizes創辦，ParkLU旨在那家立一個原創的互動平台，幫助中國消費者發掘新的品牌與商品.</p>
                        <p>ParkLU是岶克街(上海)企業管理諮詢有限公司旗下的品牌。公司地址：上海市黃浦區淮海中路98號金鐘廣場5層</p>
                    </div>
                    <div class="col-lg-2 red cc">服務簡介</div>
                    <div class="col-lg-10">
                        <p>完善合作需求<br>
                            在这开始的一步中，向KOL介绍您的品牌和产品。选择您想要合作的KOL类型和所在社交平台。<br>
                            在合作指引中您可以更详细的说明您希望KOL如何介绍您的品牌和产品。您也可以选择在每篇文章发布之前审核文章内容.
                            我们的KOL覆盖所有主流社交平台包括微薄、微信、Instagram、Nice等等。</p>
                    </div>
                    <div class="col-lg-2 red cc">平台運作方式</div>
                    <div class="col-lg-10">
                        <p>设置您的预算和产品赠送<br>
                            在这一步中，填入您希望合作开始和截止的日期，KOL数量，以及您愿意提供给KOL的现金预算.<br>
                            别担心，我们不会像KOL透露您的预算。如果您也向KOL提供产品赠送，那么可以上传产品的图片、名称并声情并明产品的零售价值和提供的数量</p>
                    </div>
                </div>

            </div>

        </div>

    </div>
</div>

<script src="<?=JS?>/layer.js"></script>
<script src="<?=JS?>/bootstrap.js"></script>
<script src="<?=JS?>/common.js"></script>
</body>
</html>