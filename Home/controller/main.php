﻿<?php
/**
 * User: 钟贵廷
 * Date: 2017/9/6
 * Time: 20:14
 */

class main extends spController
{

	/**
	 * 首页
	 */
	public function index()
	{
		$this->display("main/index.php");
	}


	/**
	 * 新闻
	 */
	public function news()
	{
		$this->display("main/news.php");
	}


	/**
	 * 关于我们
	 */
	public function info()
	{
		$this->display("main/info.php");
	}



	public function user()
	{
		$userModel = spClass('user');
		$user = $userModel->findAll();
		dump($user);
	}
	
	public function logout()
	{
		@session_destroy();
		redirect(site_url('index.php/main/login'));
	}
	
	function login()
	{
		$sys_time = time();
		if(isset($_POST['username'])) {
			$username = isset($_POST['username']) ? trim($_POST['username']) : exit(json_encode(array('status'=>false,'tips'=>' 用户名不能为空')));
			if($username=="")exit(json_encode(array('status'=>false,'tips'=>' 用户名不能为空')));
			$Member_model = spClass('times');
			$Users = spClass('users');
			//密码错误剩余重试次数
			$rtime = $Member_model->find(array('username'=>$username,'is_admin'=>1));
			$maxloginfailedtimes = 5;
			if($rtime !== false)
			{
				if($rtime['failure_times'] >= $maxloginfailedtimes) {
					$minute = 60-floor(($sys_time-$rtime['login_time'])/60);
					if($minute>0)
					{
						exit(json_encode(array('status'=>false,'tips'=>' 密码尝试次数过多，被锁定一个小时，剩余'.$minute.'分钟')));
					}
				}
			}
	
			//查询帐号，默认组1为超级管理员
			$r = $Users->find(array('username'=>$username));
			if(!$r) exit(json_encode(array('status'=>false,'tips'=>' 用户名或密码不正确')));
			$password = md5(md5(trim($_POST['password'])));
	
			$ip = $_SERVER["REMOTE_ADDR"];
			if($r['password'] != $password) {
				if($rtime['failure_times'] <= $maxloginfailedtimes && $rtime['failure_times'] > 0) {
					$times = $maxloginfailedtimes-intval($rtime['failure_times']);
					$Member_model->update(array('login_ip'=>$ip,'is_admin'=>1,'username'=>$username),array('failure_times'=>$rtime['failure_times'] +1,'login_time'=>$sys_time));
				}elseif($rtime === false||$minute<0){
					$Member_model->delete(array('username'=>$username,'is_admin'=>1));
					$Member_model->create(array('username'=>$username,'login_ip'=>$ip,'is_admin'=>1,'login_time'=>$sys_time,'failure_times'=>1));
					$times = $maxloginfailedtimes;
				}
	
				exit(json_encode(array('status'=>false,'tips'=>' 密码错误您还有'.$times.'机会')));
			}
	
			$Member_model->delete(array('username'=>$username));
			// 			if($r['status'] == 0)exit(json_encode(array('status'=>false,'tips'=>' 您的帐号已被锁定，暂时无法登录')));
			$Member_model->update(array('last_login_ip'=>$ip,'last_login_time'=>date('Y-m-d H:i:s')),array('user_id'=>$r['user_id']));
			set_userdata('user_id',$r['user_id']);
			set_userdata('user_fullname',$r['fullname']);
			set_userdata('user_name',$username);
			set_userdata('group_id',$r['group_id']);
	
			exit(json_encode(array('status'=>true,'tips'=>' 登录成功','next_url'=>"/admin.php")));
	
		}else {
			$this->require_js=true;
			$this->display("main/login.php");
		}
	}

	/**
	 * 测试程序，模板赋值
	 */
	public function test()
	{
		$this->test = '测试我的赋值方法，这里呢，需要关闭smarty,因为我觉得他要编译，这样就不够speed啦，老子是牛x的程序员，要什么模板引擎';
		$this->display("main/test.php");
	}


	public function test2()
	{
		Common_helper_student::study();
		Common_helper_teacher::teach();
	}
}	