<?php
/**
 * 跃飞科技版权所有 @2017
 * User: zhong
 * Date: 2017/9/8
 * Time: 13:30
 */
class test extends spController
{
    public function test2()
    {
        $testModel = new Common_model_test();
        $this->user = $testModel->getUser();
        $this->display('test/test2.php');
    }

    public function register()
    {
        $testModel = new Common_model_test();
        $testModel->insertUser();
    }

    public function updateUser()
    {
        $testModel = new Common_model_test();
        $res = $testModel->updateUser();
        p($res);
    }


    /**
     * 描述:学习下faker伪造数据，具体可以看这个类的哦，这个主要是用来填充数据用的吧
     * @author 钟贵廷
     * @WeChat gt845272922
     * @qq 845272922
     * @link https://github.com/gtzhong/Faker
     */
    public function addData()
    {
        $faker = Faker\Factory::create('zh_CN');
        echo $faker->name;
        echo "<br/>";
        echo $faker->address;
        echo "<br/>";
        echo $faker->text;
        echo "<br/>";
        p($faker->uuid());
//       $a =  new \Faker\Provider\Base();
//        echo $a->randomDigit();
    }
}