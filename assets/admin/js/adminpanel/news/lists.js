	define(function (require) {
	    var $ = require('jquery');
	    var aci = require('aci');
	    require('bootstrap');
	    require('bootstrapValidator');

		$(function () {
			$("#reverseBtn").click(function(){
				aci.ReverseChecked('pid[]')
			});

			$("#deleteBtn").click(function(){
				var _arr = aci.GetCheckboxValue("pid[]");
				if(_arr.length==0)
				{
					alert("请先勾选明细");
					return false;
				}
				if(confirm('确定要删除吗?'))
				{
					$("#form_list").submit();
				}
			});
        
			 $(".delete-btn").click(function(){
				var v = $(this).val();
				if(confirm('确定要删除吗?'))
				{
					window.location.href= SITE_URL+ "adminpanel/news/delete_one/"+v;
				}
			});
            
            $('#validateform').bootstrapValidator({
				message: '输入框不能为空',
				feedbackIcons: {
					valid: 'glyphicon glyphicon-ok',
					invalid: 'glyphicon glyphicon-remove',
					validating: 'glyphicon glyphicon-refresh'
				},
				fields: {
					 newstype_id: {
						 validators: {
							notEmpty: {
								message: '栏目名称不符合要求'
							}
						 }
					 },
					 title: {
						 validators: {
							notEmpty: {
								message: '标题不符合要求'
							}
						 }
					 },
					 viewcount: {
						 validators: {
							notEmpty: {
								message: '查看总数不符合要求'
							}
						 }
					 },
					 audit: {
						 validators: {
							notEmpty: {
								message: '审核状态不符合要求'
							}
						 }
					 },
					 intro: {
						 validators: {
							notEmpty: {
								message: '简介不符合要求'
							}
						 }
					 },
					 content: {
						 validators: {
							notEmpty: {
								message: '新闻内容不符合要求'
							}
						 }
					 },
					 tag: {
						 validators: {
							notEmpty: {
								message: '标签不符合要求'
							}
						 }
					 },
					 keyword: {
						 validators: {
							notEmpty: {
								message: '关键词不符合要求'
							}
						 }
					 },
					 short_title: {
						 validators: {
							notEmpty: {
								message: '短标题不符合要求'
							}
						 }
					 },
				}
			}).on('success.form.bv', function(e) {
				
				e.preventDefault();
				$("#dosubmit").attr("disabled","disabled");
				
				$.scojs_message("正在保存，请稍等...", $.scojs_message.TYPE_ERROR);
				$.ajax({
					type: "POST",
					url: edit?SITE_URL+"adminpanel/news/edit/"+id:SITE_URL+"adminpanel/news/add/",
					data:  $("#validateform").serialize(),
					success:function(response){
						var dataObj=jQuery.parseJSON(response);
						if(dataObj.status)
						{
							$.scojs_message('操作成功,3秒后将返回列表页...', $.scojs_message.TYPE_OK);
							aci.GoUrl(SITE_URL+'adminpanel/news/',1);
						}else
						{
							$.scojs_message(dataObj.tips, $.scojs_message.TYPE_ERROR);
							$("#dosubmit").removeAttr("disabled");
						}
					},
					error: function (request, status, error) {
						$.scojs_message(request.responseText, $.scojs_message.TYPE_ERROR);
						$("#dosubmit").removeAttr("disabled");
					}                  
				});
			
			}).on('error.form.bv',function(e){ $.scojs_message('带*号不能为空', $.scojs_message.TYPE_ERROR);$("#dosubmit").removeAttr("disabled");});
            
        });
});
